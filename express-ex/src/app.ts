import {MathServer} from "./mathServer";
const PORT = 3000;
const server = new MathServer(PORT);
server.listen().then(() => {
  console.log("listening on port 3000");
});