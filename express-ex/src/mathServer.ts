import * as express from "express";
import { Calculation } from "./calculation";
const calculation = new Calculation;


export class MathServer {
    private _app;
    constructor(private port: number) {
        this._app = express();
        this.routing();
    }

    private routing() {
        this._app.get("/", function(req, res) {
            res.send("Example: /3/times/4");
        });

        this._app.get("/:first/:operator/:second", (req, res, err) => {

            let operator: string = req.params.operator;
            let first: number = parseInt(req.params.first);
            let second: number = parseInt(req.params.second);

            calculation.calculation(first, second, operator)
            .then((data) => res.send(data));
            // .catch((error) => res.send(error));
            // Todo: what to do with the errors?
        });
     return this._app;
    };

    listen() {
        return new Promise((resolve, reject) => {
            try {
                this._app.listen(this.port, (err) => {
                if ( err ) reject(err);
                resolve(this._app);
            });
        } catch (e) {
            reject(e);
            }
        });
    }

    get app() {
     return this._app;
     // Error: Accessors are only available when targeting ECMAScript 5 and higher.
     // Notice that tsconfig.json is "target: es5"
    }
};


