export class Calculation {


    private errorsCheck (first, second, operator): string[] { // checks if problems in user input
    // checks the first and second number inputs and push to errors array
        let errors = [];
        if (isNaN(first) || isNaN(second)) {
           errors.push("Can't calculate this thing - it's NaN!");
        };

    // checks the operator and push to errors array
        switch (operator) {
            case "plus":
            case "minus":
            case "divided-by":
            case "times":
                break;
            default:
                errors.push("Valid actions: 'plus', 'minus', 'divided-by', 'times'");
                break;
        }
        return errors;
    };

    private doSomeMath(first: number, second: number, operator: string): number {
        let result;

        switch (operator) {
        case "plus":
            result = first + second;
            break;
        case "minus":
            result = first - second;
            break;
        case "divided-by":
            result = first / second;
            break;
        case "times":
            result = first * second;
            break;
        };
        return result;
        };

    calculation (first: number, second: number, operator: string): Promise<string> {
        let errors: string[] = this.errorsCheck(first, second, operator);

        if (errors.length !== 0) {
            return Promise.resolve(JSON.stringify({errors: errors}));
        }
        else {
            return new Promise((resolve, reject) => {
                setTimeout(() => {
                resolve(JSON.stringify({data: this.doSomeMath(first, second, operator)}));
                }, 1500);
            });
        }
    }
};
