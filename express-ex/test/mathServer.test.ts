import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import {MathServer}  from "../src/mathServer";
import * as request from "supertest";

const expect = chai.expect;
const PORT = 3000;
const server = new MathServer(PORT);
const app = server.app;
app.listen();
chai.use(chaiAsPromised);

describe("MathServer", () => {
    describe("routing", () => {
        it("should have a home page message", (done) => {
            request(app)
              .get("/")
              .expect(200)
              .end((err, res) => {
              expect(res.text).to.be.deep.equal("Example: /3/times/4");
              done();
            });
        });

        it("should respond with data", (done) => {
            request(app)
            .get("/2/plus/4")
            .expect(200)
            .end( (err, res) => {
                console.log(res.text)
                console.log(JSON.parse(res.text))
                expect(JSON.parse(res.text)).to.be.deep.equal({"data": 6});
                done();
            });
        });
        it ("should respond with error", (done) => {
            request(app)
            .get("/2/asdf/2")
            .expect(200)
            .end( (err, res) => {
                expect(JSON.parse(res.text)).to.be.deep.equal({errors: ["Valid actions: 'plus', 'minus', 'divided-by', 'times'"]});
                done();
            });
        });
    });

    describe("listen", () => {
        it("should fucking listen on the port we want him to listen to.",
        () => {
            // Todo: how do I test a try catch?
            return expect(server.listen()).to.be.fulfilled;
        });
    });
});

