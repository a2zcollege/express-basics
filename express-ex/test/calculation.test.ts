import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import {Calculation}  from "../src/calculation";

chai.use(chaiAsPromised);
chai.should();
const calculation = new Calculation;

describe("Calculation", () => {
    let validActionsErrorMessage = ["Valid actions: 'plus', 'minus', 'divided-by', 'times'"];
    const NaNErrorMessage = ["Can't calculate this thing - it's NaN!"];
    const fullErrorsList = [NaNErrorMessage, validActionsErrorMessage];

    describe("calculation()", () => {
        it("should have a 'plus' operator", () => {
            return calculation.calculation(1, 2, "plus").should.become(JSON.stringify({"data": 3}));
            });
        it("should have a 'divided-by' operator", () => {
            return calculation.calculation(4, 2, "divided-by").should.become(JSON.stringify({"data": 2}));
        });
        it("should have a 'times' operator", () => {
            return calculation.calculation(1, 2, "times").should.become(JSON.stringify({"data": 2}));
        });
        it("should have a 'minus' operator", () => {
            return calculation.calculation(7, 2, "minus").should.become(JSON.stringify({"data": 5}));
        });
        it("should have a 'Valid actions' error message", () => {
            return calculation.calculation(2, 3, "sdaf").should.become(JSON.stringify({errors: validActionsErrorMessage}));
        });
        it("should have a 'Nan message' error message", () => {
            return calculation.calculation(2, "sdf" as any, "plus").should.become(JSON.stringify({errors: NaNErrorMessage}));
        });
        // I want to check that both of the error message are working togather, 
        // but the order of the messages in the array is changing every test.
    });
});
